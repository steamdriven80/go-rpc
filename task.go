package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"html/template"
	"log"
	"net/http"
)

type Task struct {
	Name   string `json:name`
	Script string `json:script`
	Owner  string `json:owner`
}

type TaskMgr struct {
	Tasks map[string]Task
}

func (t *TaskMgr) AddTask(name string, script string, owner string) {

	task := Task{Name: name, Script: script, Owner: owner}

	if _, ok := t.Tasks[name]; ok == true {
		return
	}

	t.Tasks[name] = task

	return
}

func (t *TaskMgr) CreateOrShow(w http.ResponseWriter, r *http.Request) {

	if err := r.ParseForm(); err != nil {
		log.Fatal(err.Error())
	}

	which := mux.Vars(r)["name"]

	if _, ok := t.Tasks[which]; ok != true {
		script := r.Form.Get("script")

		if len(script) == 0 {
			script = r.PostForm.Get("script")
		}

		t.AddTask(which, r.Form.Get("script"), "chris")

		data, _ := json.Marshal(struct {
			Status  int
			Message string
		}{200, "task added successfully: " + which})

		w.Write(data)

		return
	} else {
		temp, _ := template.New("TaskDetails").Parse(`<html>
			<body>
			<div style="width:100%; text-align:center;">
			<h3>{{ .Name }}</h3> <author>{{ .Owner }}</author><br><br>
			<pre>{{ .Script }}</pre>
			</div></body></html>`)

		temp.Execute(w, t.Tasks[which])

		return
	}
}

func (t *TaskMgr) List(w http.ResponseWriter, r *http.Request) {

	details, _ := template.New("TaskDetails").Parse(`<div style="width:100%; text-align:center;"><h3>{{ .Name }}</h3> <author>{{ .Owner }}</author><br><a href="/job/{{ .Name }}/delete">Delete</a><br><form action="/job/{{.Name}}/update" method="POST"><div class="script_editor" style="left:calc(50% - 200px); width:400px; text-align:left; height:600px; border:1px solid gray;" class="editor">{{ .Script }}</div><input type='hidden' class='hidden' name='script'><input onfocus='this.parentNode.querySelector(".hidden").value = this.parentNode.querySelector(".script_editor").ace2.getValue();' type="submit" value="Update"></form></div>`)

	w.WriteHeader(200)
	w.Write([]byte(`<html><head><title>Task List</title><link href="monokai.css" rel="stylesheet"><script src="ace.js"></script><script src="mode-sh.js"></script></head><body>`))

	for _, t := range t.Tasks {
		details.Execute(w, t)
	}

	w.Write([]byte(`<script>
	
	var editors = document.querySelectorAll(".script_editor");

	for( var i = 0; i < editors.length; i++ ) {
		var item = editors[i];
    	item.ace2 = ace.edit(item);
    	item.ace2.setTheme("ace/theme/monokai");
    	item.ace2.getSession().setMode("ace/mode/sh");
    	console.log(item.ace2);
    }

</script></body></html>`))

	return
}

func (t *TaskMgr) Looking4Work(w http.ResponseWriter, r *http.Request) {

	w.WriteHeader(200)
	w.Write([]byte("<!DOCTYPE html><html><body>"))

	for _, t := range t.Tasks {
		data, _ := json.Marshal(t)

		w.Write(data)

		break
	}

	w.Write([]byte("</body></html>"))

	return
}

func (t *TaskMgr) Delete(w http.ResponseWriter, r *http.Request) {

	which := mux.Vars(r)["name"]

	delete(t.Tasks, which)

	http.Redirect(w, r, "/job", 301)
}

func (t *TaskMgr) Update(w http.ResponseWriter, r *http.Request) {

	which := mux.Vars(r)["name"]

	if err := r.ParseForm(); err != nil {
		log.Fatal(err.Error())
	}

	script := r.Form.Get("script")

	task := t.Tasks[which]

	task.Script = script

	t.Tasks[which] = task

	http.Redirect(w, r, "/job", 301)
}
