package main

import (
	"github.com/gorilla/mux"
	"html/template"
	"net/http"
)

func serveFile(w http.ResponseWriter, r *http.Request) {

	http.ServeFile(w, r, mux.Vars(r)["resource"])

}

func main() {

	t := TaskMgr{make(map[string]Task)}

	m := mux.NewRouter()

	m.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		tmp, _ := template.New("tmp").Parse("<html><body>Hello World!</body></html>")
		tmp.Execute(w, struct{}{})
	})
	m.HandleFunc("/job", t.List)
	m.HandleFunc("/job/{name}", t.CreateOrShow)
	m.HandleFunc("/job/{name}/delete", t.Delete)
	m.HandleFunc("/job/{name}/update", t.Update)
	m.HandleFunc("/l4work", t.Looking4Work)
	m.HandleFunc("/{resource}", serveFile)

	http.ListenAndServe(":8080", m)
}
